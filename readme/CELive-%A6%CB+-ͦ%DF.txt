celive使用手册

1.如何管理
  安装完成后,后台地址是http://yourdomain/celive目录/admin
  初始管理员用户/密码为你的CmsEasy后台用户密码
  *注:请及时更改管理员登录密码
2.如何更改密码及个人资料
  后台->个人资料修改,可以修改用户昵称及密码
3.如何管理客服部门
  管理员身份登录,后台->系统管理->部门管理,可以添加删除客服部门
4.如何管理客服
  管理员身份登录,后台->系统管理->客服管理,可以添加删除客服,以及设置管理员身份
5.如何任务分配
  管理员身份登录,后台->系统管理->任务管理,可以指定某个客服归属于某个部门
6.如何得到外部调用代码
  后台->系统管理->生成代码,可以指定生成模式/客服部门,以及添加QQ号码
7.如何查看聊天记录
  后台->交谈记录,非管理员身份只能查看自身的交谈记录
8.如何进行接待访客
  管理员和客服登录后,会自动切换到在线状态
  后台->接通客服,就可以开始接待,可以控制是否隐身(离线)以及声音提示
  当有访客请求时,点击接通开始会话或者拒绝
9.如何收集访客信息
  管理员身份登录,后台->系统管理->系统配置,可以开启是否收集访客信息
10.外部调用js代码(两条横线之间部分)
___________________________________________________________________________________________________
<!-- BEGIN CmsEasy Live Code, Copyright (c) 2009 CmsEasy.cn. All Rights Reserved -->
<div id="ifonline"></div>
<script type="text/javascript" language="javascript" src="http://yourdomain/celive目录/js/include.php?cmseasylive&qq=你的QQ号码,你的QQ号码"></script>
<!-- END CmsEasy Live Code, Copyright (c) 2009 CmsEasy.cn. All Rights Reserved -->
___________________________________________________________________________________________________
 *注:推荐后台生成js代码


注意事项:
1.请不要同时登陆同一账号
2.停止工作后,请点击'退出'来正确退出,切勿直接关闭后台
3.终止接待窗口的时候,请点击'结束会话'